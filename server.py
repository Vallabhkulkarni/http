import socket
import sys
import threading
from datetime import datetime, timezone
from email.utils import formatdate
import string
import os
from config import *

# store config file data, client ip , status code port number , maximum permitted connectionsrespectively
IP_CLIENT = STATUSCODE =  None
SERVERNAME = "VallabhsHttpServer/1.0"

#some of comman extensions for image referred from web : "https://blog.hubspot.com/insiders/different-types-of-image-files"
extensionsForImage = [".jpeg",".gif", ".png",".jpg",".tiff",".psd",".pdf",".eps",".ai",".indd",".cr2",".crw",".nef",".pef"]
otherFileExtensions = []

def dateInHttpFormat(Ltime=False):
    return str(formatdate(timeval=None, localtime=Ltime, usegmt=True))

def lastModifiedTime(path=""):
    lastModified = ""
    try:
        # get last modified time
        lastModifiedTimeForGivenFile = os.path.getmtime(path)
    except Exception :
        lastModifiedTimeForGivenFile = 0
    # format last modified time according to HTTP/1.1 rules
    lastModified = datetime.fromtimestamp(lastModifiedTimeForGivenFile, timezone.utc).strftime("%a, %d %b %Y %H:%M:%S") + " GMT"
    return lastModified


        
def decodeRequest(http_request):
    strHttpRequest = string(http_request)
    processed_request = strHttpRequest.split(sep = "\n")   
    
    return processed_request

def processGetHeadRequest(processed_request):
    global STATUSCODE, SERVERNAME
    try:
        temp_processed_request = processed_request[0].split()
        file_path = temp_processed_request[1]
        http_version = temp_processed_request[2]
        currentDate = dateInHttpFormat()
        
        try:
            
            if os.path.isfile(file_path):
                
                if os.access(file_path, os.R_OK):
                    
                    lastModified = lastModifiedTime(file_path)
                    fileptr = open(file_path,"r")
                    temp_file = fileptr.read()
                    contentLength = len(temp_file)
                    
                    if len(temp_file) == 0 :
                        STATUSCODE = "204 No Content"
                        temp_file = "<http>\r\n<body>\r\n<h1>204 No Content.</h1>\r\n</body>\r\n</html>\r\n"
                        contentLength = len(temp_file)
                        
                    else :
                        STATUSCODE = "200 OK"
                        
                else :
                    STATUSCODE = "403 Forbidden"
                    temp_file = "<http>\r\n<body>\r\n<h1>403 Forbidden.</h1>\r\n</body>\r\n</html>\r\n"
                    contentLength = len(temp_file)
                    lastModified = lastModifiedTime(file_path)
                
            else :
                print("file does not exist")
                STATUSCODE = "404 Not Found"
                temp_file = "<http>\r\n<body>\r\n<h1> 404 Not Found.</h1>\r\n</body>\r\n</html>\r\n\r\n"
                contentLength = len(temp_file)
                lastModified = "None"
                
        except Exception:
            STATUSCODE = "400 Bad Request"
            temp_file = "<http>\r\n<body>\r\n<h1> 400 Bad request.</h1>\r\n</body>\r\n</html>\r\n\r\n"
            contentLength = len(temp_file)
            lastModified = lastModifiedTime(file_path)
            
    except Exception:
        STATUSCODE = "400 Bad Request"
        temp_file = "<http>\r\n<body>\r\n<h1> 400 Bad request.</h1>\r\n</body>\r\n</html>\r\n\r\n"
        contentLength = len(temp_file)
        lastModified = lastModifiedTime(file_path)
        
    try :        
        if(processed_request[0] == "GET"):
            http_response =( http_version + " " + STATUSCODE +"\r\n" + "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME + 
                            "\r\n" + "Last-Modified: " + lastModified + "\r\n" + "Content-Length: " + contentLength + "\r\n" +
                            "Connection: Closed\r\n" +  temp_file + "\r\n\r\n") 
        elif (processed_request[0] == "HEAD"):
            http_response =(http_version + " " + STATUSCODE +"\r\n" + "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME + 
                            "\r\n" + "Last-Modified: " + lastModified + "\r\n" + "Content-Length: " + contentLength + "\r\n" +
                            "Connection: Closed\r\n\r\n")
    except Exception:
        http_response = ("HTTP/1.1 500 Internal Server Error\r\n"+ "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME +
                         "Connection: Closed\r\n")
    return http_response

def processDeleteRequest(processed_request):

    global STATUSCODE, SERVERNAME
    currentDate = dateInHttpFormat()
    
    try:
        temp_processed_request = processed_request[0].split()
        file_path = temp_processed_request[1]
        http_version = temp_processed_request[2]
        
        try:
            if os.path.isfile(file_path):
                
                if os.access(file_path, os.R_OK):
                    
                    fileptr = open(file_path,"r")
                    temp_file = fileptr.read()
                    try : 
                        os.remove(file_path)
            
                        if len(temp_file) == 0 :
                            STATUSCODE = "204 No Content"
                            
                        else :
                            STATUSCODE = "200 OK"
                            
                    except Exception:
                        STATUSCODE = "403 Forbidden"
                        
                else :
                    STATUSCODE = "403 Forbidden"

                
            else :
                print("file does not exist")
                STATUSCODE = "404 Not Found"
                
        except Exception:
            STATUSCODE = "400 Bad Request"
            
    except Exception:
        STATUSCODE = "400 Bad Request"
        
        
    try :
        if STATUSCODE == "200 OK":
            http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                            "Connection: Closed\r\n"+"<http>\r\n<body>\r\n<h1>File deleted.</h1>\r\n</body>\r\n</html>\r\n")
        
        elif STATUSCODE == "204 No Content":
            http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                           "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1>204 No Content.</h1>\r\n</body>\r\n</html>\r\n")
        
        elif STATUSCODE == "403 Forbidden":
            http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                           "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1>403 Forbidden.</h1>\r\n</body>\r\n</html>\r\n")
            
        elif STATUSCODE == "404 Not Found":
            http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                           "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1> 404 Not Found.</h1>\r\n</body>\r\n</html>\r\n\r\n")
                    
        elif STATUSCODE == "400 Bad Request":
            http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                            "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1> 400 Bad request.</h1>\r\n</body>\r\n</html>\r\n\r\n")
                                 
    except Exception:
        http_responce = ("HTTP/1.1 500 Internal Server Error\r\n"+ "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME + "\r\n" +
                         "Connection: Closed\r\n"+ "<http>\r\n<body>\r\n<h1> 500 Internal Server Error.</h1>\r\n</body>\r\n</html>\r\n\r\n")
        
    return http_responce

def processPutPostRequest(processed_request):
    
    global STATUSCODE, SERVERNAME
  
    temp_processed_request = processed_request[0].split()
    file_path = temp_processed_request[1]
    http_version = temp_processed_request[2]
    temp_file = ""
    currentDate = dateInHttpFormat()        
        
    if temp_processed_request[0] == "PUT" :
        if os.path.isfile(file_path):
            fileptr = open(file_path, "a")
            for i in range(4,len(processed_request)):
                temp_file += i
            if len(temp_file == 0):
                STATUSCODE = "204 No Content"
            else:
                fileptr.write(temp_file)
                STATUSCODE = "200 OK"
        
        else :
            fileptr = open(file_path, "a")
            for i in range(4,len(processed_request)):
                fileptr.write(processed_request[i])
            STATUSCODE = "201 Created"
            
    elif temp_processed_request[0] == "POST" :
        if os.path.isfile(file_path):
            fileptr = open(file_path, "a")
            for i in range(4,len(processed_request)):
                temp_file += i
            if len(temp_file == 0):
                STATUSCODE = "204 No Content"
            else:
                fileptr.write(temp_file)
                STATUSCODE = "201 OK"
        
        else :
            fileptr = open(file_path, "a")
            for i in range(4,len(processed_request)):
                fileptr.write(processed_request[i])
            STATUSCODE = "404 Not Found"
    
    if temp_processed_request[0] == "PUT" :       
        try :
            if STATUSCODE == "200 OK":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                                "Connection: Closed\r\n"+"<http>\r\n<body>\r\n<h1>File Put Successfully.</h1>\r\n</body>\r\n</html>\r\n")
            
            elif STATUSCODE == "204 No Content":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                            "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1>204 No Content.</h1>\r\n</body>\r\n</html>\r\n")
            
            elif STATUSCODE == "201 Created":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                            "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1>201 Created.</h1>\r\n</body>\r\n</html>\r\n")
                
            elif STATUSCODE == "400 Bad Request":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                                "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1> 400 Bad request.</h1>\r\n</body>\r\n</html>\r\n\r\n")
                                    
        except Exception:
            http_responce = ("HTTP/1.1 500 Internal Server Error\r\n"+ "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME + "\r\n" +
                            "Connection: Closed\r\n"+ "<http>\r\n<body>\r\n<h1> 500 Internal Server Error.</h1>\r\n</body>\r\n</html>\r\n\r\n")
            
    elif temp_processed_request[0] == "POST" :
        try :
            if STATUSCODE == "200 OK":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                                "Connection: Closed\r\n"+"<http>\r\n<body>\r\n<h1>File Post Successfully.</h1>\r\n</body>\r\n</html>\r\n")
            
            elif STATUSCODE == "204 No Content":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                            "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1>204 No Content.</h1>\r\n</body>\r\n</html>\r\n")
                
            elif STATUSCODE == "400 Bad Request":
                http_responce = (http_version + STATUSCODE +"\r\n" + currentDate + "\r\n" +"Server: "  + SERVERNAME+"\r\n"+
                                "Connection: Closed\r\n" + "<http>\r\n<body>\r\n<h1> 400 Bad request.</h1>\r\n</body>\r\n</html>\r\n\r\n")
                                    
        except Exception:
            http_responce = ("HTTP/1.1 500 Internal Server Error\r\n"+ "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME + "\r\n" +
                            "Connection: Closed\r\n"+ "<http>\r\n<body>\r\n<h1> 500 Internal Server Error.</h1>\r\n</body>\r\n</html>\r\n\r\n")
            
    return http_responce

def handleClientThread(client_Connection = None):
    global STATUSCODE


    connectionData = client_Connection.recv(1024).decode()
    processed_request = decodeRequest(connectionData)
    temp_processed_request = processed_request[0].split()
    methodOfRequest = temp_processed_request[0]
       
    currentDate = dateInHttpFormat()

    if methodOfRequest == "GET" or methodOfRequest == "HEAD":
        http_response = processGetHeadRequest(processed_request)

    elif methodOfRequest == "PUT" or methodOfRequest == "POST":
        http_response = processPutPostRequest(processed_request)
            
    elif methodOfRequest == "DELETE":
        http_response = processDeleteRequest(processed_request)

    else:
        # if request is not valid
        http_response = ("HTTP/1.1 500 Internal Server Error\r\n"+ "Date:" +currentDate + "\r\n" +  "Server: "+SERVERNAME + "\r\n" +
                        "Connection: Closed\r\n"+ "<http>\r\n<body>\r\n<h1> 500 Internal Server Error.</h1>\r\n</body>\r\n</html>\r\n\r\n")
        # send valid formed response
    client_Connection.send(http_response)
    # close connections once response sent
    client_Connection.close()


def startHttpServer():
    
    global IP_CLIENT,MAX_CONNECTIONS,PORT_NO
    
#    if IP_CLIENT == None :
#        IP_CLIENT = "0.0.0.0"
    
    if MAX_CONNECTIONS == None :
        MAX_CONNECTIONS = 30
        
    if PORT_NO == None:
        PORT_NO = 11800
    
    httpServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    try:
        # setting server port
        httpServerPort = int(PORT_NO)
    
    except Exception as error:
        # if port invalid exit the program
        print(error, "\n" + "Enter valid Port Number")
        sys.exit()
           
    httpServerSocket.bind(('127.0.0.1', httpServerPort))
    httpServerSocket.listen()
    #print("connected to server")
        
    #starting server 
    # while loop for running server for infinite loop
    while True:
       
        # if number of active connections are exceeding maximum permitted connections then discard that request
        if threading.activeCount() <= MAX_CONNECTIONS:
            # accepting client connections
            clientConnection, addr = httpServerSocket.accept()
                
            # getting clients ip address
            IP_CLIENT = str(addr[0])
            print("connected to client", IP_CLIENT)
                
            # initialising and starting thread for each connected client
            newThreadForEachConnection = threading.Thread(target=handleClientThread, args=(clientConnection, ))
            newThreadForEachConnection.start()

    

try:
    startHttpServer()
except KeyboardInterrupt:
    sys.exit()

